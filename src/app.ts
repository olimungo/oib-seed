/// <reference path="./typings/tsd.d.ts" />

import { bootstrap } from 'angular2/platform/browser';
import { provide } from 'angular2/core';
import { Shell } from './app/components/shell/shell';
import { FilteryService } from './app/components/filtery/filteryService/filtery.service';
import { OrganigramService } from './app/components/organigram/organigram.service';
import { NursesService } from './app/components/calendar/nurses/nurses.service';
import { DoctorsService } from './app/components/filterContent/playground/doctors/doctors.service';

bootstrap(Shell, [ FilteryService, OrganigramService, NursesService, DoctorsService ]);
