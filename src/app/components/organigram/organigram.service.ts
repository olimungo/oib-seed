import { Injectable } from 'angular2/core';
import { IOrganigramService } from './organigram.interface';

@Injectable()
export class OrganigramService implements IOrganigramService {
  constructor() {
  }
  
  getData(): any {
    var result: any;
    
    result = {
      label: 'Level I',
      directChildren: [
            { label: 'Direct child I.01' }
          ],
      children: [
        {
          label: 'Level II.1',
          children: [
            {
              label: 'Level III.1.1',
              children: [
                {
                  label: 'Level4.1.1.1'
                },
                {
                  label: 'Level4.1.1.2'
                },
                {
                  label: 'Level4.1.1.3'
                }
              ]
            },
            {
              label: 'Level III.1.2',
              children: [
                {
                  label: 'Level4.2.1.1'
                },
                {
                  label: 'Level4.2.1.2'
                },
                {
                  label: 'Level4.2.1.3'
                }
              ]
            }
          ]
        },
        {
          label: 'Level II.2',
          children: [
            {
              label: 'Level III.2.1',
              children: [
                {
                  label: 'Level4.2.1.1'
                },
                {
                  label: 'Level4.2.1.2'
                }
              ]
            }
          ]
        },
        {
          label: 'Level II.3',
          children: [
            {
              label: 'Level III.3.1',
              children: [
                {
                  label: 'Level4.3.1.1'
                },
                {
                  label: 'Level4.3.1.2'
                }
              ]
            }
          ]
        },
        {
          label: 'Level II.4',
          children: [
            {
              label: 'Level III.4.1',
              children: [
                {
                  label: 'Level4.3.1.1'
                },
                {
                  label: 'Level4.3.1.2'
                }
              ]
            },
            {
              label: 'Level III.4.2'
            },
            {
              label: 'Level III.4.3'
            },
            {
              label: 'Level III.4.4'
            },
            {
              label: 'Level III.4.4'
            },
            {
              label: 'Level III.4.4'
            },
            {
              label: 'Level III.4.4'
            },
            {
              label: 'Level III.4.4'
            },
            {
              label: 'Level III.4.4'
            }
          ]
        },
        {
          label: 'Level II.5',
          children: [
            {
              label: 'Level III.5.1',
              children: [
                {
                  label: 'Level4.5.1.1'
                },
                {
                  label: 'Level4.5.1.2'
                }
              ]
            }
          ]
        },
      ]
    };
    
    return result;
  }
}