import { Component, Input } from 'angular2/core';

declare var Snap: any;
declare var mina: any;

enum ReferencePoint { UP_LEFT, MIDDLE_LEFT, MIDDLE_RIGHT, BOTTOM_CENTER };

interface Coords {
  x?: number;
  y?: number;
  width?: number,
  height?: number
}

@Component({
  selector: 'oib-organigram',
  providers: [],
  templateUrl: 'app/components/organigram/organigram.html',
  styleUrls: ['app/components/organigram/organigram.css'],
  directives: [],
  pipes: []
})
export class Organigram {
  @Input() data: any;
  
  private context: any;
  private paper: any;
  private svgElement: any;
  
  ngAfterViewInit() {
    // Retrieving the SVG element from the DOM to use it later to draw SVGs using the library SnapSVG
    this.paper = Snap('#svg');
    
    // Retrieving the SVG element from the DOM in order to change the Viewbox property later
    this.svgElement = document.getElementsByTagName('svg')[0];
    
    this.drawOrganigram();
  }
  
  drawOrganigram(): void {
    var PADDING = 30; 
    var level2Entities = this.data.children || [];
    
    // Process Level 2 entities first. Level 3 entities are processed into each Level 2 draw.
    level2Entities.reduce((coords: Coords, level2Entity: any) => {
      return this.drawLevel2Entity(coords, level2Entity);
    }, { x: 0, y: 0 });
    
    var level1Coords = { x: 0, y: 0 };
    
    if (level2Entities.length > 0) {
      var width = level2Entities[level2Entities.length-1].coords.x - level2Entities[0].coords.x;
      level1Coords = {
        x: level2Entities[0].coords.width / 2,
        y: level2Entities[0].coords.y
      }
    }
    
    // Process the one and only Level 1 entity
    this.drawLevel1Entity(level1Coords, width, this.data);
    
    // Level2 entities are drawn after their connector, meaning the connector is drawn ABOVE
    // the rectangle. When the mouse hovers over the entity, the scaling-up animation reveals
    // the connector. So... reordering all the Level2 entities and putting them above the last
    // drawn entity which is the Level1 entity.
    level2Entities.map((entity: any) => {
      this.data.rect.after(entity.rect);
      entity.rect.after(entity.text);
    });
    
    // Recenter organigram
    var paperSize = this.paper.getBBox();
    this.svgElement.setAttribute('viewBox', (paperSize.x - PADDING / 2) + ' ' + (paperSize.y - PADDING) +
      ' ' + (paperSize.width + PADDING) + ' ' + (paperSize.height + PADDING * 2));
  }
  
  private drawEntity(coords: Coords, referencePoint: ReferencePoint, entity: any): void {
    var ENTITY: any = { WIDTH: 160, HEIGHT: 50, FONT_SIZE: 11 };
    
    var entityCoords: Coords = {
      x: coords.x,
      y: coords.y,
      width: ENTITY.WIDTH,
      height: ENTITY.HEIGHT
    };
    
    // Compute new coordinates depending on the reference point
    if (referencePoint === ReferencePoint.MIDDLE_RIGHT) {
      entityCoords.x = entityCoords.x - entityCoords.width;
      entityCoords.y = entityCoords.y - (entityCoords.height / 2);
    } else if (referencePoint === ReferencePoint.BOTTOM_CENTER) {
      entityCoords.x = entityCoords.x - (entityCoords.width / 2);
      entityCoords.y = entityCoords.y - entityCoords.height;
    } else if (referencePoint === ReferencePoint.MIDDLE_LEFT) {
      entityCoords.y = entityCoords.y - (entityCoords.height / 2);
    }
    
    // Assign coordinates to entity
    entity.coords = entityCoords;
    
    // Draw a box
    entity.rect = this.paper.rect(entityCoords.x, entityCoords.y, entityCoords.width, entityCoords.height);
    
    entity.rect.attr({
      fill: '#fff',
      stroke: '#000',
      strokeWidth: 1
    });
    
    // Add an effect on mouseOver
    entity.rect.mouseover(() => {
      entity.rect.animate({
        transform: 'S1.2 1.2 center'
      }, 130, mina.easein, () => {
        entity.rect.animate({
          transform: 'S1 1 center'
        }, 130, mina.easein);
      });
      
    });
    
    // Display the label in the console when entity is clicked
    // TODO: improve 
    entity.rect.click(() => {
      console.log('Click: ' +  entity.label);
    });
    
    // Add the label of the entity
    entity.text = this.paper.text(entityCoords.x, entityCoords.y, entity.label);
    
    entity.text.attr({
      'font-size': ENTITY.FONT_SIZE,
      'cursor': 'default'
    });
    
    // Display the label in the console when entity is clicked
    // TODO: improve 
    entity.text.click(() => {
      console.log('Click: ' +  entity.label);
    });
    
    // Center the text in the box
    var textBBox = entity.text.getBBox();
    entity.text.transform('t' + ((ENTITY.WIDTH - textBBox.width) / 2) + ', ' + ((ENTITY.HEIGHT / 2) + (textBBox.height / 3)));
  }

  private drawLevel1Entity(coords: Coords, width: number, entity: any): void {
    var connectorHeight = this.drawLevel1Connector(coords.x, coords.y, width);
    
    var coordsDirectChildren: Coords = {
      x: coords.x + (width / 2),
      y: coords.y - connectorHeight
    };
    
    if (entity.directChildren && entity.directChildren.length > 0) {
      coordsDirectChildren = this.drawDirectChildren(coordsDirectChildren, entity.directChildren);
    }
    
    this.drawEntity(coordsDirectChildren, ReferencePoint.BOTTOM_CENTER, entity);
  }
  
  private drawDirectChildren(coords: Coords, entities: any): Coords {
    var lastCoords = entities.reverse().reduce((coords, entity, index, entities) => {
      var drawLeft = Boolean((entities.length - index) % 2);
      
      return this.drawDirectChildEntity(coords, entity, drawLeft);
    }, coords);
    
    return lastCoords;
  }
  
  private drawDirectChildEntity(coords: Coords, entity: any, drawLeft: Boolean): Coords {
    var connectorHeight = 0;
    
    var connectorWidth = this.drawDirectChildConnector(coords, drawLeft);
    
    var coordsEntity: Coords = {
      x: coords.x + connectorWidth,
      y: coords.y
    };
    
    var referencePoint: ReferencePoint = ReferencePoint.MIDDLE_LEFT;
    
    if (drawLeft) {
      referencePoint = ReferencePoint.MIDDLE_RIGHT;
      connectorHeight = this.drawDirectChildConnector2(coords);
    }
    
    this.drawEntity(coordsEntity, referencePoint, entity);
    
    return { x: coords.x, y: coords.y - connectorHeight };  
  }
  
  private drawDirectChildConnector(coords: Coords, drawLeft: Boolean): number {
    var CONNECTOR_WIDTH = 30;
    
    if (drawLeft) {
      CONNECTOR_WIDTH = -CONNECTOR_WIDTH;
    }
    
    var line = this.paper.line(coords.x, coords.y, coords.x + CONNECTOR_WIDTH, coords.y);
    
    line.attr({
      fill: 'none',
      stroke: '#000',
      strokeWidth: 1
    });
    
    return CONNECTOR_WIDTH;  
  }
  
  private drawDirectChildConnector2(coords: Coords): number {
    var CONNECTOR_HEIGHT = 60;
    
    var line = this.paper.line(coords.x, coords.y, coords.x, coords.y - CONNECTOR_HEIGHT);
    
    line.attr({
      fill: 'none',
      stroke: '#000',
      strokeWidth: 1
    });
    
    return CONNECTOR_HEIGHT;  
  }
  
  private drawLevel1Connector(x: number, y: number, width): number {
    var CONNECTOR_HEIGHT = 40;
    
    var line = this.paper.line(x, y, x + width, y);
    
    line.attr({
      fill: 'none',
      stroke: '#000',
      strokeWidth: 1
    });
    
    line = this.paper.line(x + (width / 2), y, x + (width / 2), y - CONNECTOR_HEIGHT);
    
    line.attr({
      fill: 'none',
      stroke: '#000',
      strokeWidth: 1
    });
    
    return CONNECTOR_HEIGHT;
  }
    
  private drawLevel2Entity(coords: Coords, level2Entity: any): Coords {
    var LEVEL2_GAP = 60;
    var LEVEL3_BACK = 20;
    
    this.drawEntity(coords, ReferencePoint.UP_LEFT, level2Entity);
    
    var connectorHeight = this.drawLevel2Connector(coords.x + (level2Entity.coords.width / 2), coords.y);
    level2Entity.coords.y = level2Entity.coords.y - connectorHeight;
    level2Entity.coords.height = level2Entity.coords.height + connectorHeight;
 
    level2Entity.children.reduce((coords: Coords, level3Entity: any) => {
      return this.drawLevel3Entity(coords, level3Entity);
    }, { x: coords.x + level2Entity.coords.width - LEVEL3_BACK, y: level2Entity.coords.y + level2Entity.coords.height });
    
    return { x: level2Entity.coords.x + level2Entity.coords.width + LEVEL2_GAP, y: coords.y };
  }
    
  private drawLevel2Connector(x: number, y: number): number {
    var CONNECTOR_HEIGHT = 30;
    var line = this.paper.line(x, y, x, y - CONNECTOR_HEIGHT);
    
    line.attr({
      fill: 'none',
      stroke: '#000',
      strokeWidth: 1
    });
    
    return CONNECTOR_HEIGHT;
  }

  private drawLevel3Entity(coords: Coords, entity: any): Coords {
    var entityCoords = this.drawLevel3Connector(coords);
    this.drawEntity(entityCoords, ReferencePoint.MIDDLE_RIGHT, entity);
    
    return { x: coords.x, y: entityCoords.y };
  }
    
  private drawLevel3Connector(coords: Coords): Coords {   
    var CONNECTOR_HEIGHT = 70;
    var CONNECTOR_WIDTH = 20;
    
    var line = this.paper.polyline(coords.x, coords.y, coords.x,  coords.y + CONNECTOR_HEIGHT, coords.x - CONNECTOR_WIDTH, coords.y + CONNECTOR_HEIGHT);
    
    line.attr({
      fill: 'none',
      stroke: '#000',
      strokeWidth: 1
    });
    
    return {
      x: coords.x - CONNECTOR_WIDTH,
      y: coords.y + CONNECTOR_HEIGHT
    };
  }
}
