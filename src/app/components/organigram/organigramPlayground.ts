import { Component } from 'angular2/core';
import { Organigram } from './organigram';
import { OrganigramService } from './organigram.service';

declare var $: any; 

@Component({
  selector: 'oib-organigram-playground',
  providers: [],
  templateUrl: 'app/components/organigram/organigramPlayground.html',
  styleUrls: ['app/components/organigram/organigramPlayground.css'],
  directives: [ Organigram ],
  pipes: []
})
export class OrganigramPlayground {
  data: any;
  
  constructor(private organigramService: OrganigramService) {
    this.data = this.organigramService.getData();
  }
}
