import { Component } from 'angular2/core';
import { IPeople } from './filteryService/filtery.interface';
import { FilterType, EType } from './filterType/filterType';
import { FilterName } from './filterName/filterName';
import { FilteredContainer } from './filteredContainer/filteredContainer';
import { FilteryService } from '../filtery/filteryService/filtery.service';

@Component({
  selector: 'oib-filtery',
  providers: [],
  templateUrl: 'app/components/filtery/filtery.html',
  styleUrls: ['app/components/filtery/filtery.css'],
  directives: [ FilterType, FilterName, FilteredContainer ],
  pipes: []
})
export class Filtery {
  peopleGroupAutocompletion: IPeople[];
  peopleGroup1: IPeople[];
  peopleGroup2: IPeople[];
  peopleGroup3: IPeople[];
  isFilterNameDisabled: boolean = false;
  
  private originalPeopleGroup1: IPeople[];
  private originalPeopleGroup2: IPeople[];
  private originalPeopleGroup3: IPeople[];
  private typeDate: string = '';
  private filterPattern: string = '';
  private type: EType = EType.ALL;
  
  constructor(private filteryService: FilteryService) {
    this.getGroups();
    this.filterGroups();
    this.peopleGroupAutocompletion = this.originalPeopleGroup1;
  }
  
  typeChanged(type: EType): void {
    this.type = type;
    this.isFilterNameDisabled = type === EType.DATE;
    this.filterGroups();
  }
  
  dateChanged(date: string): void {
    this.typeDate = date;
    this.filterGroups();
  }
  
  patternChanged(pattern: string): void {
    this.filterPattern = pattern;
    this.filterGroups();
  }
  
  tabFilteryChanged(tab: number): void {
    if (tab === 1) {
      this.peopleGroupAutocompletion = this.originalPeopleGroup1;
    } else if (tab === 2) {
      this.peopleGroupAutocompletion = this.originalPeopleGroup2;
    } else {
      this.peopleGroupAutocompletion = this.originalPeopleGroup3;
    }
  }
  
  private getGroups(): void {
    this.originalPeopleGroup1 = this.filteryService.getPeople();
    this.originalPeopleGroup2 = this.filteryService.getPeople();
    this.originalPeopleGroup3 = this.filteryService.getPeople();
  }
  
  private filterGroups(): void {
    if (this.type === EType.FAVOURITES) {
      this.peopleGroup1 = this.filterFavourites(this.originalPeopleGroup1);
      this.peopleGroup2 = this.filterFavourites(this.originalPeopleGroup2);
      this.peopleGroup3 = this.filterFavourites(this.originalPeopleGroup3);
    } else {
      this.peopleGroup1 = this.originalPeopleGroup1;
      this.peopleGroup2 = this.originalPeopleGroup2;
      this.peopleGroup3 = this.originalPeopleGroup3;
    }
    
    if (this.type === EType.DATE) {
      this.peopleGroup1 = this.filterByDate(this.peopleGroup1);
      this.peopleGroup2 = this.filterByDate(this.peopleGroup2);
      this.peopleGroup3 = this.filterByDate(this.peopleGroup3);
    } else {
      this.peopleGroup1 = this.filterByPattern(this.peopleGroup1);
      this.peopleGroup2 = this.filterByPattern(this.peopleGroup2);
      this.peopleGroup3 = this.filterByPattern(this.peopleGroup3);
    }
  }
  
  private filterFavourites(people: IPeople[]): IPeople[] { 
    return people.filter((person: IPeople) => person.isFavourite);
  }
  
  private filterByDate(people: IPeople[]): IPeople[] {
    var result = people;
    
    if (this.typeDate) {
      result = people.filter((person: IPeople) => person.birthDate === this.typeDate);
    }
    
    return result;
  }
  
  private filterByPattern(people: IPeople[]): IPeople[] {
    var result = people;
    
    if (this.filterPattern) {
      result = people.filter((person: IPeople) => person.fullName.toLowerCase().indexOf(this.filterPattern.toLowerCase()) > -1);
    }
    
    return result;
  }
}
