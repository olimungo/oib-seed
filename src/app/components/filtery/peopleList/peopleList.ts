import { Component, Input } from 'angular2/core';
import { IPeople } from '../filteryService/filtery.interface';

@Component({
  selector: 'people-list',
  providers: [],
  templateUrl: 'app/components/filtery/peopleList/peopleList.html',
  styleUrls: ['app/components/filtery/peopleList/peopleList.css'],
  directives: [],
  pipes: []
})
export class PeopleList {
  @Input() people: IPeople[];
}
