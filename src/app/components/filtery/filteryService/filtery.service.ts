import { Injectable } from 'angular2/core';
import { IFilteryService, IPeople } from './filtery.interface';

declare var faker: any;

@Injectable()
export class FilteryService implements IFilteryService {
  getPeople(): IPeople[] {
    var COUNT = 100,
        result: IPeople[] = [];
    
    for(var i = 0; i < COUNT; i++) {
      var birthDate = faker.date.between(new Date(1950, 0, 1), new Date(2010, 12, 31)),
          birthDateDay = ('00' + birthDate.getDate()).substr(-2, 2),
          birthDateMonth = ('00' + (birthDate.getMonth() + 1)).substr(-2, 2);
          
      result.push({
        id: faker.random.uuid(),
        fullName: faker.name.findName(),
        birthDate: birthDateDay + '/' + birthDateMonth + '/' + birthDate.getFullYear(),
        isFavourite: faker.random.boolean(),
        job: faker.name.jobType(),
        avatar: faker.internet.avatar()
      });
    }
    
    return result;
  }
}