import { Component } from 'angular2/core';
import { Filtery } from '../filtery/filtery';
import { OrganigramPlayground } from '../organigram/organigramPlayground';
import { AchievementBadges } from '../achievementBadge/achievementBadges';
import { CalendarPlayground } from '../calendar/playground/playground';
import { FilterContentPlayground } from '../filterContent/playground/playground';

@Component({
  selector: 'shell',
  providers: [],
  templateUrl: 'app/components/shell/shell.html',
  styleUrls: ['app/components/shell/shell.css'],
  directives: [ Filtery, OrganigramPlayground, AchievementBadges, CalendarPlayground, FilterContentPlayground ],
  pipes: []
})
export class Shell {
  currentTab: number = 5;
  
  constructor() {
  }
  
  tabChanged(tab: number): void {
    this.currentTab = tab;
  }
}
