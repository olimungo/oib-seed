import { Component } from 'angular2/core';
import { AchievementBadge } from './achievementBadge';

declare var $: any; 

@Component({
  selector: 'oib-achievement-badges',
  providers: [],
  templateUrl: 'app/components/achievementBadge/achievementBadges.html',
  styleUrls: ['app/components/achievementBadge/achievementBadges.css'],
  directives: [ AchievementBadge ],
  pipes: []
})
export class AchievementBadges {
  percentage: number = 50;
  
  ngAfterViewInit() {
    $('.slider').slider({ value: this.percentage }).on('slide', (event) => {
      this.percentage = event.value;
    });
  }
}
