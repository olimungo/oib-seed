import { Component, Input } from 'angular2/core';

@Component({
  selector: 'achievement-badge',
  providers: [],
  templateUrl: 'app/components/achievementBadge/achievementBadge.html',
  styleUrls: ['app/components/achievementBadge/achievementBadge.css'],
  directives: [],
  pipes: []
})
export class AchievementBadge {
  @Input() percentage: number = 0;
  @Input() big: any;
  @Input() small: any;
  @Input() orange: any;
  @Input() green: any;
  @Input() dark: any;
  
  classes: string = '';
  
  ngOnInit() {
    if (this.big !== undefined) {
      this.classes += 'big ';
    }
    
    if (this.small !== undefined) {
      this.classes += 'small ';
    }
    
    if (this.orange !== undefined) {
      this.classes += 'orange ';
    }
    
    if (this.green !== undefined) {
      this.classes += 'green ';
    }
    
    if (this.dark !== undefined) {
      this.classes += 'dark ';
    }
  }
}
