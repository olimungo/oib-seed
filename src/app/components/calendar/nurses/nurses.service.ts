import { Injectable } from 'angular2/core';
import { INursesService, INurse } from './nurses.interface';

declare var faker: any;

@Injectable()
export class NursesService implements INursesService {
  constructor() {
  }
  
  getData(year: number, month: number, day: number): any {
    // Year, month and day are not used here because it's a mock-up service which
    // returns fake data. In a real usage, they would be used in the call to the back-end side.
    var result: INurse[] = [],
        nurse: INurse;
    
    var total = Math.round(Math.random() * 3) + 1;
    
    for(var i = 0; i < total; i++) {
      nurse = {
        fullName: faker.name.findName(),
        isPresent: faker.random.boolean()
      };
      
      result.push(nurse);
    }
    
    return result;
  }
}