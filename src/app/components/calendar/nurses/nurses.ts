import { Component, Input } from 'angular2/core';
import { NursesService } from './nurses.service';
import { Template } from '../template/template';

@Component({
  selector: 'oib-nurses',
  providers: [],
  templateUrl: 'app/components/calendar/nurses/nurses.html',
  styleUrls: ['app/components/calendar/nurses/nurses.css'],
  directives: [],
  pipes: []
})
export class Nurses extends Template {
  nurses: any;
  date: number;
  
  constructor(private nursesService: NursesService) {
    super();
  }
  
  updateData() {
    this.date = this._day;
    this.nurses = this.nursesService.getData(this._year, this._month, this._day);
  }
}
