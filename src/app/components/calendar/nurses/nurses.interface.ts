export interface INurse {
  fullName: string;
  isPresent: boolean;
}

export interface INursesService {
  getData(year: number, month: number, day: number): any;
}