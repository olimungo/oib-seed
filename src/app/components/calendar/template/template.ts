import { Component, Input } from 'angular2/core';
import { ITemplate } from './template.interface';

@Component({
})
export class Template implements ITemplate {
  // The 3 #Input annotations will allow to set an attribute on the HTML tag to components extending this class. 
  @Input() set year(value: number) {
    this._year = value;
    
    if (this._month !== undefined && this._day !== undefined) {
      this.updateData();
    }
  }
  
  @Input() set month(value: number) {
    this._month = value;
    
    if (this._year !== undefined && this._day !== undefined) {
      this.updateData();
    }
  }
  
  @Input() set day(value: number) {
    this._day = value;
    
    if (this._year !== undefined && this._month !== undefined) {
      this.updateData();
    }
  }
  
  // This 3 properties will be available in components extending this class.
  _year: number;
  _month: number;
  _day: number;
  
  // Redeclare this function in components extending this class.
  // It will be triggered each time year, month or day are modified.
  updateData(): void {
  }
}
