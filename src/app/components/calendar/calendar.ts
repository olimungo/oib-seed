import { Component, Input, Output, ContentChild, TemplateRef, EventEmitter } from 'angular2/core';

@Component({
  selector: 'oib-calendar',
  providers: [],
  templateUrl: 'app/components/calendar/calendar.html',
  styleUrls: ['app/components/calendar/calendar.css'],
  directives: [],
  pipes: []
})
export class Calendar {
  @Input() year: number;
  @Input() month: number;
  @Input('cell-width') cellWidth: number;
  @Input('display-weekend') displayWeekend: Boolean;
  
  @Output() yearChange = new EventEmitter();
  @Output() monthChange = new EventEmitter();
  
  @ContentChild(TemplateRef) day: TemplateRef;
  
  days: number[];
  dummyDays: any[];
  calendarWidth: number;
  monthLabel: string = '';
  yearLabel: number;
  dayLabels: string[];
  shortDayLabels = [ 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN' ];

  private MONTH_LABELS = [
    'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
    'October', 'November', 'December'
  ];
  private DAY_LABELS = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' ];
  
  private date: Date = new Date();
  
  ngOnInit() {
    if (this.month !== undefined && this.year !== undefined) {
      this.date = new Date(this.year, this.month-1, 1);
    }
    
    this.setCalendar();
  }
  
  changeMonth(step: number) {
    // Setting the day of the month to the 5th because setMonth adds or removes
    // more or less 30 days depending on the case. For example, setting a date to
    // 31/03/2015 and removing 1 month gives a result of 03/03/2015.
    this.date.setDate(5);
    this.date.setMonth(this.date.getMonth() + step);
    
    this.yearChange.emit(this.date.getFullYear());
    this.monthChange.emit(this.date.getMonth() + 1);
    
    this.setCalendar();
  }
  
  private setCalendar() {
    var lastDayOfMonth: number,
        firstDayOfWeek,
        cellCount = 7,
        i;
    
    lastDayOfMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    firstDayOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    
    this.days = Array.apply(null, { length: lastDayOfMonth }).map(Number.call, (value) => {
      return Number(value + 1);
    });
    
    // Copy the array of day labels
    this.dayLabels = this.DAY_LABELS.filter((label: string) => true);
    
    // If display-weekend attribute is explicitly set to false, do not show
    // Sundays and Saturdays
    if (this.displayWeekend !== undefined && !this.displayWeekend) {
      cellCount = 5;
      
      // Remove Saturday and Sunday from labels
      this.dayLabels.splice(5, 2);
      
      // Remove Saturdays and Sundays from days
      this.days = this.days.filter((day: number) => {
        var day = new Date(this.date.getFullYear(), this.date.getMonth(), day).getDay();
        return day !== 6 && day !== 0;
      });
    }
    
    // Set calendar width based on cell width
    this.calendarWidth = this.cellWidth * cellCount;
    
    // Set Sunday as the 7th day of the week instead of 0
    if (firstDayOfWeek === 0) {
      firstDayOfWeek = 7;
    }
    
    // Insert leading dummy days
    this.dummyDays = [];
    
    for (i = 1; i < firstDayOfWeek; i++) {
      this.dummyDays.push(0);
    }
    
    this.monthLabel = this.MONTH_LABELS[this.date.getMonth()];
    this.yearLabel = this.date.getFullYear();
  }
}
