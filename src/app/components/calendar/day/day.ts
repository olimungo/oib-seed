import { Component, Input } from 'angular2/core';
import { Template } from '../template/template';

@Component({
  selector: 'oib-day',
  providers: [],
  templateUrl: 'app/components/calendar/day/day.html',
  styleUrls: ['app/components/calendar/day/day.css'],
  directives: [],
  pipes: []
})
export class Day extends Template {
}
