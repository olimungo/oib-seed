import { Component, Input } from 'angular2/core';
import { Calendar } from '../calendar';
import { Nurses } from '../nurses/nurses';
import { Day } from '../day/day';

@Component({
  selector: 'oib-calendar-playground',
  providers: [],
  templateUrl: 'app/components/calendar/playground/playground.html',
  styleUrls: ['app/components/calendar/playground/playground.css'],
  directives: [ Calendar, Nurses, Day ],
  pipes: []
})
export class CalendarPlayground {
  yearCalendar1: number = 2016;
  monthCalendar1: number = 1;
  yearCalendar2: number = 2016;
  monthCalendar2: number = 1;
  yearCalendar3: number = 2016;
  monthCalendar3: number = 1;
}
