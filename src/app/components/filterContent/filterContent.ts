import { Component, Input, Output, EventEmitter } from 'angular2/core';
import { IItem } from './filterContent.interface';
import { FilterContentPipe } from './filterContent.pipe';

@Component({
  selector: 'oib-filter-content',
  providers: [],
  templateUrl: 'app/components/filterContent/filterContent.html',
  styleUrls: ['app/components/filterContent/filterContent.css'],
  directives: [],
  pipes: [ FilterContentPipe ]
})
export class FilterContent {
  @Input() items: IItem[];
  @Input() height: number;
  
  @Output('on-item-selected') onItemSelected = new EventEmitter();
  
  itemSelected(item: IItem): void {
    this.onItemSelected.emit(item);
    
    if (item.actionUrl !== undefined) {
      // TODO: remove the following message and execute the default action.
      // Note: IHMO, no action should be triggered and the responsability should be
      //       left to the component using the oib-filter-content which is notified by the emit above.
      console.log('I\'m the oib-filter-content and I\'ve just been clicked. I should follow this URL: ' + item.actionUrl);
    }
  }
}
