import { Pipe, PipeTransform } from 'angular2/core';
import { IItem, } from './filterContent.interface';

@Pipe({
  name: 'filterContent'
})
export class FilterContentPipe implements PipeTransform {
  transform(items: IItem[], args: string[]): any {
    var result: IItem[] = items,
        pattern = args[0];
    
    if (pattern !== undefined) { 
      pattern = pattern.toLowerCase();
      
      result = result.filter((item: IItem) => {
        var expr = (item.type + ' ' + item.label + ' ' + item.description).toLowerCase();
        
        return expr.match(pattern) !== null;
      })
    }
    
    return result;
  }
}