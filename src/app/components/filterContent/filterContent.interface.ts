export interface IItem {
  type: string;
  id: string;
  label: string;
  description: string;
  actionUrl?: string,
  originalObject?: any;
}