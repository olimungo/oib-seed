import { Injectable } from 'angular2/core';
import { IDoctorsService, IDoctor } from './doctors.interface';

declare var faker: any;

@Injectable()
export class DoctorsService implements IDoctorsService {
  constructor() {
  }
  
  getDoctors(): IDoctor[] {
    var result: IDoctor[] = [],
        people: IDoctor;
    
    var COUNT = 100;
    
    for(var i = 0; i < COUNT; i++) {
      people = {
        id: faker.random.uuid(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        address: faker.address.streetAddress(),
      };
      
      result.push(people);
    }
    
    return result;
  }
}