export interface IDoctor {
  id: string;
  firstName: string;
  lastName: string;
  address: string;
}

export interface IDoctorsService {
  getDoctors(): IDoctor[];
}