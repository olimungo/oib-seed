import { Component, Input } from 'angular2/core';
import { FilterContent } from '../filterContent';
import { IDoctor } from './doctors/doctors.interface';
import { IItem } from '../filterContent.interface';
import { DoctorsService } from './doctors/doctors.service';

@Component({
  selector: 'oib-filter-content-playground',
  providers: [],
  templateUrl: 'app/components/filterContent/playground/playground.html',
  styleUrls: ['app/components/filterContent/playground/playground.css'],
  directives: [ FilterContent ],
  pipes: []
})
export class FilterContentPlayground {
  doctors: IItem[];
  
  constructor(private doctorService: DoctorsService) {
  }
  
  ngOnInit() {
    this.doctors = this.doctorService.getDoctors().map((doctor: IDoctor) => {
      return {
        type: 'DOCTOR',
        id: doctor.id,
        label: doctor.firstName + ' ' + doctor.lastName,
        description: doctor.address,
        actionUrl: '/doctor/' + doctor.id,
        originalObject: doctor
      };
    });
  }
  
  itemSelected(item: IItem): void {
    console.log('I\'m the oib-filter-conter-playground and I just received an event from a child in my DOM', item);
  }
}
